/**
 * Genera mi json web token 
 * en forma de promesa para 
 * utilizar el callback
 */

const jwt = require('jsonwebtoken')
const moment = require('moment')


/**
 * Generacion de Token Basica
 * @param {user id} uid 
 * @returns 
 */
const generarJWT = (uid = '') => {

    return new Promise((resolve, reject) => {

        const payload = { uid }

        jwt.sign(payload, process.env.SECRETORPRIVATEKEY, {

            expiresIn: '4h'

        }, (err, token) => {

            if (err) {
                console.error(err)
                reject('No se pudo generar el TOKEN')

            } else {

                resolve(token)

            }
        })
    })
}


/**
 * Creacion de Token con
 * mayor informacion y seguridad
 * @param {usuario} user 
 * @returns 
 */
const createAccessToken = (user) => {

    const { id, correo, rol } = user

    return new Promise((resolve, reject) => {
        const payload = {
            uid: id,
            correo,
            rol,
            createToken: moment().unix()
        }

        jwt.sign(payload, process.env.SECRETORPRIVATEKEY, {

            expiresIn: '4h'

        }, (err, token) => {

            if (err) {
                console.error(err)
                reject('No se pudo generar el TOKEN')

            } else {

                resolve(token)

            }
        })
    })
}


/**
 * Creacion de refresToken
 * con mayor seguridad
 * @param {usuario} user 
 * @returns 
 */
const createRefreshToken = (user) => {

    const { id } = user

    return new Promise((resolve, reject) => {

        const payload = {
            uid: id,
        }

        jwt.sign(payload, process.env.SECRETORPRIVATEKEY, {

            expiresIn: '5h'

        }, (err, token) => {

            if (err) {
                console.error(err)
                reject('No se pudo generar el TOKEN')

            } else {

                resolve(token)

            }
        })
    })
}


const checkExpiredToken = (token) => {

    try {
        const { uid, exp } = jwt.decode(token)
        const currentDate = moment().unix()
        const status = (currentDate > exp) ? true : false

        return {
            uid,
            status
        }

    } catch (error) {
        return {
            uid: null,
            status: true,
            msg: error
        }
    }
}


module.exports = {
    generarJWT,
    createAccessToken,
    createRefreshToken,
    checkExpiredToken
}