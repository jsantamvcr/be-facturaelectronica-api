const {
    User: Usuario,
    Category,
    Role,
    Product,
    Menu,
    Newslatter,
    Post
} = require("../models");

/**
 * validar si el rol existe en la base de datos
 * o no esta vacio.
 * @param {Rol} rol a validar
 */
const validRole = async (rol = "") => {
    const rolExist = await Role.findOne({ rol });
    if (!rolExist) {
        throw new Error(`Rol: ${rol} no es valido`);
    }
};

/**
 * Valida si el correo ya existe
 * @param {correo} correo a validar
 */
const emailExist = async (correo = "") => {
    const emailExist = await Usuario.findOne({ correo: correo.toLowerCase() });
    if (emailExist) {
        throw new Error(`El correo: ${correo} ya existe`);
    }
};

/**
 * Valida si una URL del post a crear ya existe
 * @param {String} url 
 */
const urlPostExist = async (url = "") => {
    const urlPostExist = await Post.findOne({ url: url.toLowerCase() });
    if (urlPostExist) {
        throw new Error(`El url del post: [${urlPostExist.url}] ya existe`);
    }
};


/**
 * Valida si el post existe por id
 * @param {String} id 
 */
const postExist = async (id = "") => {
    const urlPostExist = await Post.findById(id);
    if (!urlPostExist) {
        throw new Error(`El post: ${id} no existe`);
    }
};

/**
 * Valida si el correo ya existe
 * para un newslatter
 * @param {String} email a validar
 */
const emailExistNewslatter = async (email = "") => {
    const emailExist = await Newslatter.findOne({ email: email.toLowerCase() });
    if (emailExist) {
        throw new Error(`El correo: ${email} ya existe`);
    }
};

/**
 * Validar si el MENU existe
 * @param {String} id delmenu
 */
const menuExistById = async (id = "") => {
    const menuExist = await Menu.findById(id);
    if (!menuExist) {
        throw new Error(`El id menu: ${id} no existe`);
    }
};

/**
 * Valida si existe un USUARIO
 * @param {String} id
 */
const userExistById = async (id = "") => {
    const userExist = await Usuario.findById(id);
    if (!userExist) {
        throw new Error(`El id usuario: ${id} no existe`);
    }
};

/**
 * Valida que exista la categoria por id
 * @param {id} id de la categoria
 */
const existeCategoria = async (id) => {
    const result = await Category.findById(id);

    if (!result) {
        throw new Error(`No existe la categoria con el id ${id}`);
    }
};

/**
 * Validacion del producto por id
 * @param {id} id del producto
 */
const existeProducto = async (id) => {
    const result = await Product.findById(id);
    console.log(result);
    if (!result) {
        throw new Error(`No existe el producto con el id ${id}`);
    }
};

/**
 * Validar colecciones permitidas
 */
const coleccionPermitidas = (coleccion = "", colecciones = []) => {
    const incluida = colecciones.includes(coleccion);
    if (!incluida) {
        throw new Error(
            `La coleccion ${coleccion} no es permitida, Permitdas: ${colecciones}`
        );
    }

    return true;
};

module.exports = {
    coleccionPermitidas,
    emailExist,
    existeCategoria,
    existeProducto,
    menuExistById,
    userExistById,
    validRole,
    emailExistNewslatter,
    urlPostExist,
    postExist
};
