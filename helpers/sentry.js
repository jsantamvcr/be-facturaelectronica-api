const Sentry = require("@sentry/browser");
const { Integrations } = require("@sentry/tracing");

Sentry.init({
    dsn: "https://fd1f5d01ad2b49478f1b5b5e890451a1@o564328.ingest.sentry.io/5705013",
    integrations: [new Integrations.BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
});


const logSentryError = (error, url) => {
    console.log('estoy en sentry')
    Sentry.withScope(scope => {        
        scope.setExtra('url', url);
        Sentry.captureException(error);
    });
}

module.exports = {
    logSentryError
}