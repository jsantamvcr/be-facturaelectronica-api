//UNIFICACION de archivos para las importaciones
//al llamarse index.js, por consiguiente si apunto a la
//carpeta de middlewares, por default busca index.js

const validarArchivo = require("./validar-archivo");
const validarCampos = require("./validar-campos");
const validarJWT = require("./validar-jwt");
const validaRoles = require("./validar-roles");
const validCourses = require('./validar-courses')

module.exports = {
  ...validarArchivo,
  ...validarCampos,
  ...validCourses,
  ...validarJWT,
  ...validaRoles,
};
