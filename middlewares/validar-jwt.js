require('colors')
const { response } = require('express')
const jwt = require('jsonwebtoken')
const Usuario = require('../models/users')
const { checkExpiredToken } = require('../helpers/generarjwt')


/**
 * Valida el Token del usuario
 * que esta logueado.
 * @param {request} req 
 * @param {response} res 
 * @param {next} next 
 * @returns 
 */
const validarJWT = async (req, res = response, next) => {

    //aca colocamos x-token
    //pero puede ser otro nombre
    //queda a discricion del desarrollador
    //esto va en el header del postman
    const token = req.header('x-token')
    const { status } = checkExpiredToken(token)

    //Valido que envien el TOKEN
    if (!token) { return res.status(401).json({ msg: 'No hay token en la peticion' }) }

    if (status) { return res.status(403).json({ msg: 'Token esta vencido' }) }

    //Valido que sean un token valido, DEL USUARIO logueado
    try {

        const { uid } = jwt.verify(token, process.env.SECRETORPRIVATEKEY)

        //Leemos el usuario que modifica uid
        const usuario = await Usuario.findById(uid)

        if (!usuario) {
            return res.status(401).json({
                msg: 'Token no valido - usuario no existe en DB'
            })
        }

        //Verificar si el uid tiene estado en true
        if (!usuario.estado === true) {
            return res.status(401).json({
                msg: 'Token no valido - usuario estado Inactivo'
            })
        }

        //Cargo el Request para mantener los datos del usuario 
        // autenticado.
        req.usuario = usuario
        next()
    } catch (error) {
        //Este error lo podemos almacenar en DB o archivo
        //para verlo mas facil
        console.warn('ADVERTENCIA con el TOKEN', `${error}`.bgRed)
        res.status(401).json({
            msg: `Token no valido ${error}`
        })
    }
}


module.exports = {
    validarJWT,
}