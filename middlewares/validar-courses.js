const { response } = require("express")



const esIdCourseValido = (req, res = response, next) => {

    const {idCourse} = req.body

    //Validacion si algun campo esta vacio
    if (!idCourse.length > 0) {
        return res.status(400).json({ msg: `El Curso Id no puede estar vacio` })
    }

    next()
}



module.exports = {
    esIdCourseValido
}