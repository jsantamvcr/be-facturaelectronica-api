const { Schema, model } = require('mongoose')

const MenuSchema = Schema({
    title: String,
    url: String,
    order: Number,
    active: Boolean
})


module.exports = model('Menu', MenuSchema)