const { Schema, model } = require("mongoose");

const CourseSchema = Schema({
  idCourse: {
    type: Number,
    unique: true
  },
  link: String,
  coupon: String,
  price: Number,
  order: Number,
  estado: {
    type: Boolean,
    default: true
  },
});

/**
 * Sobre escrivimos el metod to Json para
 * devuelva un id bonito
 */
CourseSchema.methods.toJSON = function () {
  //quito los campos que no quiero que salgan
  const { __v, _id, ..._course } = this.toObject();

  //transformamos el id
  _course.uid = _id;
  return _course;
};


module.exports = model("Course", CourseSchema);