/**
 * Path unico para utilizar los modelos
 */

const Category = require('./category')
const User = require('./users')
const Product = require('./product')
const Role = require('./role')
const Server = require('./server')
const Menu = require('./menu')
const Newslatter = require('./newslatter')
const Course = require('./course')
const Post = require('./post')
module.exports = {
    Category,
    Product,
    Role,
    Server,
    User,
    Menu,
    Newslatter,
    Course,
    Post
}