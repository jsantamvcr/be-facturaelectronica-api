const { Schema, model } = require("mongoose");

const NewslatterSchema = Schema({
  email: {
    type: String,
    unique: true,
    required: [true, "email es requerido"],
  },
});

/**
 * Sobre escrivimos el metod to Json para
 * devuelva un id bonito
 */
 NewslatterSchema.methods.toJSON = function () {
   //quito los campos que no quiero que salgan
  const { __v, _id, ..._newslatter } = this.toObject();

  //transformamos el id
  _newslatter.uid = _id;
  return _newslatter;
};


module.exports = model("Newslatter", NewslatterSchema);
