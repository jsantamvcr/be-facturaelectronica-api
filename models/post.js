const { Schema, model } = require("mongoose");
var mongoosePaginate = require('mongoose-paginate');

const PostSchema = Schema({
    title: String,
    url: {
        type: String,
        unique: true,
    },
    description: String,
    date: Date
});

/**
 * Sobre escrivimos el metod to Json para
 * devuelva un id bonito
 */
PostSchema.methods.toJSON = function () {
    //quito los campos que no quiero que salgan
    const { __v, _id, ..._post } = this.toObject();

    //transformamos el id
    _post.uid = _id;
    return _post;
};

PostSchema.plugin(mongoosePaginate)
module.exports = model("Post", PostSchema);
