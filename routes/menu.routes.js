const { Router } = require('express')
const { check } = require('express-validator')
const { addMenu, getMenus, updateMenu, deleteMenu } = require('../controllers/index')

const {
    validarCampos,
    validarJWT,
    esAdminRole,
    tieneRole
} = require('../middlewares')
const { menuExistById } = require('../helpers/db-validators')

const router = Router()

router.post('/add-menu', [validarJWT], addMenu)
router.get('/get-menu', getMenus)

router.put('/:id', [
    validarJWT,
    check('id', 'No es un Id valido').isMongoId(),
    check('id').custom(menuExistById),
    validarCampos
], updateMenu)

router.delete('/:id', [
    validarJWT,
    check('id', 'No es un Id valido').isMongoId(),
    check('id').custom(menuExistById),
    validarCampos
], deleteMenu)

module.exports = router
