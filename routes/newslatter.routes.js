const { Router } = require("express");
const { subscribeEmail } = require("../controllers");
const { emailExistNewslatter } = require("../helpers");
const { validarCampos } = require("../middlewares");
const { check } = require("express-validator");
const router = Router();

router.post("/:email", [
    check('email').custom(emailExistNewslatter),
    check('email', 'Correo: formato invalido').isEmail(),
    validarCampos
], subscribeEmail);

module.exports = router;
