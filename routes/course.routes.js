const { Router } = require("express");
const { check } = require("express-validator");
const { validarCampos, validarJWT } = require("../middlewares");
const {
    addCourse,
    getCourses,
    putCourse,
    delCourse,
} = require("../controllers");
const router = Router();

router.post("/", [validarJWT,
    check("idCourse", "El id curso es obligatorio").not().isEmpty(),
    validarCampos,
], addCourse
);

router.get("/", getCourses);
router.put("/:id", [validarJWT, validarCampos], putCourse);
router.delete("/:id", [validarJWT, validarCampos], delCourse);

module.exports = router;
