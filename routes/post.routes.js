const { Router } = require("express");
const { validarJWT, validarCampos } = require("../middlewares");
const { check } = require("express-validator");
const { addPost, getPosts, deletePost, updatePost, getPostById } = require("../controllers");
const { urlPostExist, postExist } = require("../helpers");
// const { route } = require("./auth.routes");
const router = Router();

router.post("/", [validarJWT,
    check('url').custom(urlPostExist),
    validarCampos
], addPost);


router.get('/', getPosts)
router.get('/getpost', getPostById)


router.put("/:id", [validarJWT,
    check('id', 'No es un Id de Mongo').isMongoId(),
    check('id').custom(postExist),
    validarCampos
], updatePost)

router.delete("/:id", [validarJWT,
    check('id', 'No es un Id de Mongo').isMongoId(),
    check('id').custom(postExist),
    validarCampos
], deletePost)

module.exports = router;
