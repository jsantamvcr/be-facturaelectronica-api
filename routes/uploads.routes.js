const { Router } = require('express')
const { check } = require('express-validator')
const { validarCampos, validarArchivo } = require('../middlewares')

const { cargarArchivo,
    uploadAvatar,
    actualizarArchivo,
    obtenerImagen,
    actualizarArchivoClodinary } = require('../controllers')
const { coleccionPermitidas } = require('../helpers')


//Enrutador
const router = Router()

router.post('/', validarArchivo, cargarArchivo)

router.put('/:coleccion/:id', [
    validarArchivo,
    check('id', 'Id debe de ser de Mongo').isMongoId(),
    check('coleccion').custom(c => coleccionPermitidas(c, ['usuarios', 'productos'])),
    validarCampos
], actualizarArchivoClodinary)
// ], actualizarArchivo)

/**
 * Sube un avatar para el usuario o para el 
 * producto.
 */
router.put('/avatar/:coleccion/:id', [
    validarArchivo,
    check('id', 'Id debe de ser de Mongo').isMongoId(),
    check('coleccion').custom(c => coleccionPermitidas(c, ['usuarios', 'productos'])),
    validarCampos
], uploadAvatar)


router.get('/:coleccion/:id', [
    check('id', 'Id debe de ser de Mongo').isMongoId(),
    check('coleccion').custom(c => coleccionPermitidas(c, ['usuarios', 'productos'])),
    validarCampos
], obtenerImagen)



module.exports = router

