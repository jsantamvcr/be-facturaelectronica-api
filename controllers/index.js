/**
 * indice comuno para los controladores
 */

const auth = require("../controllers/auth.controller");
const category = require("../controllers/category.controller");
const product = require("../controllers/product.controller");
const user = require("../controllers/users.controller");
const buscar = require("../controllers/buscar.controller");
const uploads = require("../controllers/uploads.controller");
const menu = require("../controllers/menu.controller");
const newslatter = require("./newslatter.controller");
const course = require('./course.controller')
const post = require('./post.controller')

module.exports = {
  ...auth,
  ...buscar,
  ...category,
  ...menu,
  ...newslatter,
  ...product,
  ...uploads,
  ...user,
  ...course,
  ...post
};
