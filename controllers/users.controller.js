const { response } = require('express')
const bcrypt = require('bcryptjs');
const Usuario = require('../models/users');


const usersGet = async (req, res = response) => {

    //Este es cuando enviamos los parametros
    //Por medio de un <query>
    //const { q, nombre = 'No name', apiKey, page = 1, limit = 10 } = req.query
    const { limite = 5, desde = 0, estado = true } = req.query
    const querie = { estado }

    /** Codigo ejecutado en dos pasos */
    // const usuarios = await Usuario.find(querie)
    //     .skip(Number(desde))
    //     .limit(Number(limite))
    // const total = await Usuario.countDocuments(querie)

    //Hacemos una promesa para optimizar la ejecucion en base de datos
    //se ejecuta simultanamente
    //Realizamos una desestructuracion de ARREGLOS
    const [total, usuarios] = await Promise.all([
        Usuario.countDocuments(querie),
        Usuario.find(querie)
            .skip(Number(desde))
            .limit(Number(limite))
    ])

    res.json({
        total,
        usuarios
    })
}


/**
 * Actualizar un usuario por Id
 */
const usersPut = async (req, res = response) => {

    //id a actualizar
    const { id } = req.params
    //desestructuramos para exlcuir lo que no quiero utilizar
    const { _id, password, google, correo, ...user } = req.body;

    //Encryptar el password
    if (password) {
        const salt = bcrypt.genSaltSync()
        user.password = bcrypt.hashSync(password, salt)
    }

    //Guardar
    await Usuario.findByIdAndUpdate(id, user)
    const usuario = await Usuario.findById(id)

    res.status(200).json({
        usuario
    })
}

/**Almacenando 
 * la informacion a base de datos
 */
const usersPost = async (req, res = response) => {
    try {
        //<resto>, es para enviar los demas campos a actualizar
        // en caso de que se requiera
        //se hace de esta forma si queremos desestructurar los campos 
        //que son obligatorios
        const { nombre, correo, password, rol, lastname, ...resto } = req.body;
        const user = new Usuario({
            nombre,
            lastname,
            correo: correo.toLowerCase(),
            password,
            rol
        })

        //Encryptar el password
        const salt = bcrypt.genSaltSync()
        user.password = bcrypt.hashSync(password, salt)

        //Guardar los datos        
        await user.save()

        //esto es lo que devuelvo
        res.status(200).json({
            msg: 'Usuario creado correctamente',
            correo: user.correo
        })
    } catch (err) {
        console.log(err)
    }
}


/**
 * para la creacion de usuarios por primera vez
 * @param {*} req 
 * @param {*} res 
 */
const singUp = async (req, res = response) => {
    try {
        const { correo, password, privacyPolicy, rol, ...resto } = req.body;
        const user = new Usuario({
            correo: correo.toLowerCase(),
            password,
            privacyPolicy,
            rol
        })

        const salt = bcrypt.genSaltSync()
        user.password = bcrypt.hashSync(password, salt)

        await user.save()

        res.status(200).json({ msg: `usuario ${correo} creado correctamente` })

    } catch (err) {
        res.status(500).json({ msg: `existe un error ${err}` })
    }
}

const usersPatch = (req, res = response) => {
    res.json({
        msg: "usersPatch API - Controlador"
    })
}

const usersDelete = async (req, res = response) => {

    const { id } = req.params

    //Borrado Logico
    const usuario = await Usuario.findByIdAndUpdate(id, { estado: false })

    res.json({ usuario })
}


module.exports = {
    usersGet,
    usersDelete,
    usersPatch,
    usersPut,
    usersPost,
    singUp
}



