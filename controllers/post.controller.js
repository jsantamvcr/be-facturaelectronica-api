const { response } = require("express");
const { Post } = require("../models");

/**
 * Add New Post
 * @param {*} req 
 * @param {*} res 
 */
const addPost = async (req, res = response) => {
    try {
        const _post = req.body;
        const post = new Post(_post);
        const data = await post.save();

        res.status(200).json({
            msg: "Post almacenado correctamente",
            code: 200,
            data
        })
    } catch (error) {
        res.status(500).json({
            msg: 'Error en el Servidor',
            code: 500,
            data: error
        })
    }
};


/**
 * Get all post
 * @param {*} req 
 * @param {*} res 
 */
const getPosts = async (req, res = response) => {
    const { page = 1, limit = 10 } = req.query

    const option = {
        page,
        limit: parseInt(limit),
        sort: { date: "desc" }
    }

    Post.paginate({}, option, (err, postStored) => {
        if (err) {
            res.status(500).send({
                code: 500,
                msg: "Error del servidor"
            })
        } else {
            if (!postStored) {
                res.status(404).send({
                    code: 404,
                    msg: "No hay post"
                })
            } else {
                res.status(200).json({
                    code: 200,
                    posts: postStored
                })
            }
        }
    })
}


const getPostById = async (req, res = response) => {
    try {
        const { url } = req.query
        const posts = await Post.findOne({ url })

        if (posts) {
            res.status(200).json({
                code: 200,
                url,
                posts
            })
        } else {
            res.status(404).json({
                code: 404,
                msg: 'No existe el Post'
            })
        }

    } catch (error) {
        res.status(500).json({
            code: 500,
            error
        })
    }
}

const updatePost = async (req, res = response) => {
    try {
        const { id } = req.params
        const body = req.body
        await Post.findByIdAndUpdate(id, body)
        res.status(200).json({
            msg: 'Update Post',
            code: 200,
            body
        })
    } catch (error) {
        res.status(500).json({
            msg: error,
            code: 500
        })
    }
}

const deletePost = async (req, res = response) => {
    try {
        const { id } = req.params
        await Post.findByIdAndDelete(id)
        res.status(200).json({
            msg: 'Post eliminado correctamente',
            code: 200
        })
    } catch (error) {
        res.status(500).json({
            msg: error,
            code: 500
        })
    }

}

module.exports = {
    addPost,
    getPosts,
    updatePost,
    deletePost,
    getPostById
};
