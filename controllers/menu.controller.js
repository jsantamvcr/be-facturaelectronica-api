const { response } = require("express");
const Menu = require("../models/menu");

const addMenu = (req, res = response) => {
    const { title, url, order, active } = req.body;
    const menu = new Menu({ title, url, order, active });

    menu.save((error, createManu) => {
        if (error) {
            res.status(500).send({ msg: `Error de servidor ${error})` });
        } else {
            if (!createManu) {
                res.status(404).send({ msg: "Error al crear el menu." });
            } else {
                res.status(200).send({ msg: "Menu creado correctamente" });
            }
        }
    });
};

const getMenus = (req, res = response) => {
    const { limite = 5, desde = 0, active } = req.query;
    const querie = active ? { active } : null;

    Menu.find(querie)
        .sort({ order: "asc" })
        .skip(Number(desde))
        .limit(Number(limite))
        .exec((error, result) => {
            if (error) {
                res.status(500).send({ msg: `Error de servidor ${error})` });
            } else {
                if (!result) {
                    res.status(404).send({ msg: "Error al crear el menu." });
                } else {
                    res.status(200).send({
                        menus: result,
                    });
                }
            }
        });
};

const updateMenu = (req, res = response) => {
    let menuData = req.body;
    const params = req.params;

    Menu.findByIdAndUpdate(params.id, menuData, (error, result) => {
        if (error) {
            res.status(500).send({ msg: `Error de servidor ${error})` });
        } else {
            if (!result) {
                res.status(404).send({ msg: "Error al actualizar el menu." });
            } else {
                res.status(200).send({
                    msg: "Menu actualizado correctamente",
                    menu: result,
                });
            }
        }
    });
};

const deleteMenu = (req, res = response) => {
    const { id } = req.params;
    Menu.findByIdAndDelete(id, (error, result) => {
        if (error) {
            res.status(500).send({ msg: `Error de servidor ${error})` });
        } else {
            if (!result) {
                res.status(404).send({ msg: "Error al eliminar el menu." });
            } else {
                res.status(200).send({
                    msg: "Menu eliminado correctamente",
                    menu: result.title,
                });
            }
        }
    });
};

module.exports = {
    addMenu,
    getMenus,
    updateMenu,
    deleteMenu,
};
