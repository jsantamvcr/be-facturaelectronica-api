const { response } = require("express");
const bcryptjs = require("bcryptjs");
const User = require("../models/users");
const {
  createAccessToken,
  createRefreshToken,
  checkExpiredToken,
} = require("../helpers/generarjwt");
const { googleVerify } = require("../helpers/google-verify");

const login = async (req, res = response) => {
  const { correo, password } = req.body;

  try {
    // validar correo existe y usuario activo
    const usuario = await User.findOne({ correo: correo.toLowerCase() });

    if (!usuario) {
      return res.status(401).json({
        msg: "El usuario / password no son correctos - correo",
      });
    }

    if (!usuario.estado) {
      return res.status(401).json({
        msg: "El usuario / password no son correctos - estado: inactivo",
      });
    }

    // validar la contrase;a
    const validPassword = bcryptjs.compareSync(password, usuario.password);
    if (!validPassword) {
      return res.status(401).json({
        msg: "El usuario / password no son correctos - password",
      });
    }

    // Generar JWT
    const token = await createAccessToken(usuario);
    const refreshToken = await createRefreshToken(usuario);

    res.json({
      token,
      refreshToken,
    });
  } catch (err) {
    console.log(`${err}`.red);
    return res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

const googleSingIn = async (req, res = response) => {
  const { id_token } = req.body;

  try {
    const { correo, nombre, img } = await googleVerify(id_token);

    let usuario = await User.findOne({ correo });

    if (!usuario) {
      //crear usuario
      const data = {
        nombre,
        correo,
        password: ":P",
        img,
        google: true,
      };

      usuario = new User(data);
      await usuario.save();
    }

    //Si el usuario en DB  status false
    if (!usuario.estado) {
      return res.status(401).json({
        msg: `Hable con el administrador. Usuario bloqueado`,
      });
    }

    const token = await createAccessToken(usuario);
    const refreshToken = await createRefreshToken(usuario);

    res.json({
      usuario,
      token,
      refreshToken,
    });
  } catch (error) {
    res.status(400).json({
      msg: `Token de google invalido: ${error.message}}`,
    });
  }
};

const refreshToken = async (req, res = response) => {
  const { refreshToken } = req.body;
  const { uid, status } = checkExpiredToken(refreshToken);

  if (status) {
    return res.status(401).json({ msg: "Token ya expiro" });
  }

  const usuario = await User.findById(uid);

  if (!usuario) {
    return res
      .status(401)
      .json({ msg: "El usuario / password no son correctos - correo" });
  }

  if (!usuario.estado) {
    return res
      .status(401)
      .json({ msg: "El usuario / password no son correctos - estado: false" });
  }

  const token = await createAccessToken(usuario);

  res.json({
    token,
    refreshToken,
  });
};

module.exports = {
  login,
  googleSingIn,
  refreshToken,
};
