const { response } = require("express");
const { Newslatter } = require("../models");

const subscribeEmail = async (req, res = response) => {
  const { email } = req.params;

  const newslatter = new Newslatter({
    email: email.toLowerCase(),
  });

  const data = await newslatter.save();

  res.json(data);
};

module.exports = {
  subscribeEmail,
};
