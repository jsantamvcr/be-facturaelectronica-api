const { response } = require("express");
const { Course } = require("../models");


/**
 * agregamos un nuevo curso
 */
const addCourse = async (req, res = response) => {
    try {
        const body = req.body
        const course = new Course(body)
        course.order = 1000

        //Validacion del curso existe
        const existCourse = await Course.findOne({ idCourse: body.idCourse })
        if (existCourse) {
            return res.status(400).json({ msg: `El Curso Id: ${existCourse.idCourse} ya existe` })
        }

        const result = await course.save()
        res.status(200).json({ msg: 'Curso creado correctamente', result })

    } catch (error) {
        res.status(500).json({ msg: 'No se ha podido crear el curso', result: error })
    }
}

/**
 * se obtiene los cursos de manera
 * ordenada. 
 * @param {Object} req 
 * @param {object} res 
 */
const getCourses = async (req, res = response) => {
    try {
        const { limite = 5, desde = 0, estado } = req.query;
        const querie = { estado }

        const [total, result] = await Promise.all([
            Course.countDocuments(querie),
            Course.find(querie)
                .sort({ order: "asc" })
                .skip(Number(desde))
                .limit(Number(limite))
        ])

        res.status(200).json({
            msg: "Listado correctamente",
            total,
            result
        })
    } catch (error) {
        res.status(500).json({
            msg: "Error con el API",            
            error
        })
    }
}

/**
 * Actualizo el curso
 * @param {Object} req 
 * @param {Object} res 
 */
const putCourse = async (req, res = response) => {
    let data = req.body;
    const params = req.params;

    Course.findByIdAndUpdate(params.id, data, (error, result) => {
        if (error) {
            res.status(500).send({ msg: `Error de servidor ${error})` });
        } else {
            if (!result) {
                res.status(404).send({ msg: "Error al actualizar el course." });
            } else {
                res.status(200).send({
                    msg: "Curso actualizado correctamente",
                    result,
                });
            }
        }
    });
}

/**
 * Elimino el curso
 * @param {Object} req 
 * @param {Object} res 
 */
const delCourse = async (req, res = response) => {
    const params = req.params;
    const data = { estado: false }

    Course.findByIdAndDelete(params.id, data, (error, result) => {
        if (error) {
            res.status(500).send({ msg: `Error de servidor ${error})` });
        } else {
            if (!result) {
                res.status(404).send({ msg: "Error al actualizar el course." });
            } else {
                res.status(200).send({
                    msg: "Curso eliminado correctamente",
                    result,
                });
            }
        }
    });
}


module.exports = {
    addCourse,
    getCourses,
    putCourse,
    delCourse
}
